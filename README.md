![coverage](https://gitlab.com/guillaumeduveau/docker-php7-cli/badges/master/build.svg)

This PHP image,based on Alpine, is meant for dev purposes only. You can use it instead of installing those tools on your metal system :

+ PHP 7 in FPM mode, for coupling with an Apache / NGINX container
+ runs PHP-FPM with UID=1000 and GID=1000 (which is generally corresponding to the user/group of your own user in Ubuntu, so you don't have any permission problems)
+ the usual PHP extensions for Drupal with some extensions meant for PHP-CLI usage
+ Composer, Drush and Drupal console
+ does not have xDebug enabled, in order to speed up PHP-CLI scripts
+ has utils like MC, nano, mysql-client...
+ NOT FOR PRODUCTION
