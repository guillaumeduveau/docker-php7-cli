FROM alpine:3.6
LABEL maintainer "guillaume.duveau@gmail.com"

# Ensure www-data user exists
RUN addgroup -g 82 -S www-data && \
    adduser -u 82 -D -S -G www-data www-data

# Upgrade
RUN apk upgrade --no-cache

# Install general tools
RUN apk add --no-cache \
            curl \
            git \
            mc \
            mysql-client \
            nano \
            wget \
            zsh

# Install PHP with extensions
RUN apk add --no-cache \
            php7 \
            php7-bcmath \
            php7-dom \
            php7-ctype \
            php7-curl \
            php7-fileinfo \
            php7-fpm \
            php7-gd \
            php7-iconv \
            php7-intl \
            php7-json \
            php7-mbstring \
            php7-mcrypt \
            php7-mysqlnd \
            php7-opcache \
            php7-openssl \
            php7-pdo \
            php7-pdo_mysql \
            php7-pdo_pgsql \
            php7-pdo_sqlite \
            php7-phar \
            php7-posix \
            php7-session \
            php7-simplexml \
            php7-soap \
            php7-tokenizer \
            php7-xml \
            php7-xmlreader \
            php7-xmlwriter \
            php7-zip \
            php7-zlib

# Configure PHP-FPM
RUN sed -i 's/;daemonize = yes/daemonize = no/g' /etc/php7/php-fpm.conf && \
    sed -i 's/listen = 127.0.0.1:9000/listen = [::]:9000/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/user = nobody/user = www-data/g' /etc/php7/php-fpm.d/www.conf && \
    sed -i 's/group = nobody/group = www-data/g' /etc/php7/php-fpm.d/www.conf

# Run PHP-FPM as www-data with different UID=1000 and GID=1000
RUN deluser www-data && \
    addgroup -g 1000 -S www-data && \
    adduser -u 1000 -D -S -s /bin/bash -G www-data www-data && \
    sed -i '/^www-data/s/!/*/' /etc/shadow

# Configure PHP
COPY etc/php7/conf.d/*.ini /etc/php7/conf.d/

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Drush, you will need to drush init at 1st login
USER www-data
RUN composer global require drush/drush && \
    composer clear-cache

# Allow to launch term apps
ENV TERM xterm

# Install Drupal Console
USER root
RUN curl https://drupalconsole.com/installer -L -o drupal.phar && \
    mv drupal.phar /usr/local/bin/drupal && \
    chmod +x /usr/local/bin/drupal

# Below, all is bad :(
# Docker entrypoint
COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["php-fpm7"]
